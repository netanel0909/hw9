#include "BSNode.h"
#include <iostream>
#include <string>
#include <fstream>

BSNode::BSNode(string data)//c'tor
{
	this->_right = 0;
	this->_data = data;
	this->_left = 0;
	this->_count = 1;
}

BSNode::BSNode(const BSNode& other)//c'tor
{
	*this = other;
}

BSNode::~BSNode()//d'tor
{
	if (_left) delete _left;
	if(_right) delete _right; 
}

int BSNode::getDepth(const BSNode& root) const//getters
{
	return getCurrNodeDistFromInputNode(&root);
}

std::string BSNode::getData() const
{
	return _data;
}

BSNode* BSNode::getLeft() const
{
	return _left;
}

BSNode* BSNode::getRight() const
{
	return _right;
}

bool BSNode::isLeaf() const
{
	return (!(_left || _right));
}

BSNode& BSNode::operator=(const BSNode& other)//deep copy
{
	this->_count = other._count;
	this->_data = other._data;

	if (other._right) 
		this->_right = new BSNode(*other._right);
	else 
		this->_right = 0;

	if (other._left) 
		this->_left = new BSNode(*other._left);
	else 
		this->_left = 0;

	return *this;

}

int BSNode::getHeight() const//getter
{
	int h = 0, rh = 0, lh = 0;
	if (!isLeaf())
	{
		if (getLeft())
		{
			lh = getLeft()->getHeight() + 1;
		}
		if (getRight())
		{
			rh = getRight()->getHeight() + 1;
		}
		h = lh < rh ? rh : lh;
	}
	return h;

}

void BSNode::insert(std::string value)//inserts val to tree if no avilable leaf make a new one
{
	if (this->_data > value)
	{
		if (_left) _left->insert(value);
		else _left = new BSNode(value);
	}
	else if (value> this->_data)
	{
		if (_right) _right->insert(value);
		else _right = new BSNode(value);
	}
	else
	{
		this->_count++;
	}
}

bool BSNode::search(std::string val) const//search for val in tree (yes-true no-false)
{
	bool return_val = false;

	if (! val.compare(this->_data))
	{
		return_val = true;
	}
	else if (!this->isLeaf())
	{
		if (getRight())
		{
			return_val = return_val || this->getRight()->search(val);
		}
		if (getLeft())
		{
			return_val = return_val || this->getLeft()->search(val);
		}
	}
	return return_val;

}

void BSNode::printNodes() const //prints the nodes
{
	if (_right) _right->printNodes();
	std::cout << _data << " " << _count << std::endl;
	if (_left) _left->printNodes();

}

int BSNode::getCurrNodeDistFromInputNode(const BSNode* node) const//gets hight
{
	int dis = 0;
	if (!(this->getData().compare(node->getData()) == 0))
	{
		if (node->getRight()->search(this->getData()) && node->getRight() == 0)
		{
			dis = getCurrNodeDistFromInputNode(node->getLeft()) + 1;
		
		}
		else
		{
			dis = getCurrNodeDistFromInputNode(node->getRight()) + 1;
		}
	}
	return dis;

}

