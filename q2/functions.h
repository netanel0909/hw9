#pragma once

template<typename T>
inline int compare(T x, T y)
{
	if (x < y) return 1;
	if (x > y) return -1;
	return 0;
}

template<class T>
void swap(T* a, T* b)
{
	T temp = *a;
	*a = *b;
	*b = temp;
}

template<class T>
void bubbleSort(T* arr, int size)
{
	if (size == 1) return;

	for (int i = 0; i<size - 1; i++)
	{
		if (arr[i] > arr[i + 1]) swap(&arr[i], &arr[i + 1]);
	}

	bubbleSort(arr, size - 1);
}

template<class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}