#include "functions.h"
#include "newClass.h"
#include <iostream>



int main() {

	newClass bin0 = newClass(0,0);
	newClass bin1 = newClass(0,1);
	newClass bin2 = newClass(1,0);
	newClass bin3 = newClass(1,1);

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//char check
	std::cout << "sould print -1, 1, 0" << std::endl;
	std::cout << compare('a', 'A') << std::endl;
	std::cout << compare('A', 'a') << std::endl;
	std::cout << compare('a', 'a') << std::endl;

	//check bin
	std::cout << "sould print 1 -1 0" << std::endl;
	std::cout << compare(bin1, bin2) << std::endl;
	std::cout << compare(bin3, bin0) << std::endl;
	std::cout << compare(bin3, bin3) << std::endl;

	////check bubbleSort
	
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	

	//char check
	std::cout << "correct print is sorted array" << std::endl;

	char charArr[6] = { 'A', 'b', 't', 'K', 'z', 'Z' };
	bubbleSort<char>(charArr, arr_size);
	for (int i = 0; i < 6; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check bin
	std::cout << "correct print is sorted array" << std::endl;

	newClass arr[4] = { bin1,bin3, bin0, bin2 };
	bubbleSort<newClass>(arr, 4);
	for (int i = 0; i < 4; i++) {
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;
	
	//char check

	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, 6);
	std::cout << std::endl;

	//char bin

	std::cout << "correct print is sorted array" << std::endl;
	printArray<newClass>(arr, 4);
	std::cout << std::endl;

	system("pause");
	return 1;
}