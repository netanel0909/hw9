#include<iostream>
#pragma once
class newClass
{
private:


public:
	bool msb;
	bool lsb;

	newClass();
	newClass(bool, bool);
	~newClass();

	bool operator <(const newClass& temp);
	bool operator >(const newClass& temp);
	bool operator ==(const newClass& temp);
	bool operator =(const newClass& temp);

	friend std::ostream& operator<<(std::ostream& os, const newClass& temp);

};