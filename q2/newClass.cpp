#include "newClass.h"



newClass::newClass()
{
}

newClass::newClass(bool msb_n, bool lsb_n)
{
	this->msb = msb_n;
	this->lsb = lsb_n;
}


newClass::~newClass()
{
}

bool newClass::operator<(const newClass & temp)
{
	if (this->msb < temp.msb) return true;
	else if (this->msb == temp.msb)
	{
		if (this->lsb < temp.lsb)
		{
			return true;
		}
	}
	return false;
}

bool newClass::operator>(const newClass & temp)
{
	if (this->msb > temp.msb) return true;
	else if (this->msb == temp.msb)
	{
		if (this->lsb > temp.lsb)
		{
			return true;
		}
	}
	return false;
}

bool newClass::operator==(const newClass & temp)
{
	return (this->msb == temp.msb && this->lsb == temp.lsb);
}

bool newClass::operator=(const newClass & temp)
{
	this->msb = temp.msb;
	this->lsb = temp.lsb;

	return true;
}

std::ostream & operator<<(std::ostream & os, const newClass & temp)
{
	os << temp.msb << temp.lsb;
	return os;
}
