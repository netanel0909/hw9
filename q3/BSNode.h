#ifndef BSNode_H
#define BSNode_H

#include <string>

using namespace std;

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode<T>& other);

	~BSNode();
	
	void insert(T value);
	BSNode<T>& operator=(const BSNode<T>& other);

	bool isLeaf() const;
	T getData() const;
	BSNode<T>* getLeft() const;
	BSNode<T>* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode<T>& root) const;

	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode<T>* node) const; //auxiliary function for getDepth

};

template<class T>
BSNode<T>::BSNode(T data)//c'tor
{
	this->_data = data;
	this->_left = 0;
	this->_right = 0;
	this->_count = 1;

}
template<class T>
BSNode<T>::BSNode(const BSNode<T>& other)//c'tor
{
	*this = other;
}

template<class T>
BSNode<T>::~BSNode()//d'tor
{
	if (_right) delete _right;
	if (_left) delete _left;
}
template<class T>
bool BSNode<T>::isLeaf() const//check if leaf
{
	return (_left == 0 && _right == 0);
}
template<class T>
void BSNode<T>::insert(T value)//inserts a val to tree
{
	if (value < this->_data)
	{
		if (_left) _left->insert(value);
		else _left = new BSNode<T>(value); 
	}
	else if (value > this->_data)
	{
		if (_right) _right->insert(value); 

		else _right = new BSNode<T>(value); 

	}
	else this->_count++;

}
template<class T>
BSNode<T>& BSNode<T>::operator=(const BSNode<T>& other)//deep cpy
{
	this->_count = other.getCount();
	this->_data = string(other.getData());

	if (other._left) this->_left = new BSNode<T>(*other.getLeft());
	else this->_left = 0;


	if (other._right) this->_right = new BSNode<T>(*other.getRight());
	else this->_right = 0;

	return *this;

}
template<class T>
bool BSNode<T>::search(T val) const//search val in tree (yes-true no- false)
{
	bool return_val = false;
	if (_data == val)
	{
		return_val = true;
	}
	else if (_data >= val)
	{
		if (_left)
		{
			return_val = _left->search(val);
		}
	}
	else if (_right)
	{
		return_val = _right->search(val); 
	}

	return return_val;
}
template<class T>
void BSNode<T>::printNodes() const//prints tree to screen
{
	if (_right) _right->printNodes();
	std::cout << _data << " " << _count << std::endl;
	if (_left) _left->printNodes();
}
template<class T>
int BSNode<T>::getHeight() const//getters
{
	int h = 0, rh = 0, lh = 0;
	if (!isLeaf())
	{
		if (getLeft())
		{
			lh = 1 + getLeft()->getHeight();
		}
		if (getRight())
		{
			rh = 1 + getRight()->getHeight();
		}
	}
	return rh < lh ? lh : rh;


}
template<class T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode<T>* node) const//auxiliary function for getDepth
{
	int dis = 0;
	if (!(this->getData() == node->getData()))
	{
		if (node->getRight() != 0 && node->getRight()->search(this->getData()))
		{
			dis = 1 + getCurrNodeDistFromInputNode(node->getRight());
		}
		else
		{
			dis = 1 + getCurrNodeDistFromInputNode(node->getLeft());
		}
	}
	return dis;


}
template<class T>
int BSNode<T>::getDepth(const BSNode<T>& root) const//getters
{
	return getCurrNodeDistFromInputNode(&root);
}
template<class T>
T BSNode<T>::getData() const
{
	return _data;
}
template<class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return _left;
}
template<class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return _right;
}



#endif