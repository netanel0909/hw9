#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>

using std::string;
using std::cout;
using std::endl;

int main()
{
	int arr[3] = { 1, 2, 3 };

	for (int i = 0; i < 3; i++) cout << arr[i] << " ";

	cout << endl;
	BSNode<int> tree(arr[0]);
	for (int i = 1; i < 3; i++) tree.insert(arr[i]);

	tree.printNodes();
	cout << endl;


	string arr2[3] = {"hi", "im", "perry"};
	for (int i = 0; i < 3; i++) cout << arr2[i] << " ";

	cout << endl;
	BSNode<string> bs2(arr2[0]);
	for (int i = 1; i < 3; i++) bs2.insert(arr2[i]);

	bs2.printNodes();
	cout << endl;

	return 0;
}

